import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/app.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/environment.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/local_storage.dart';

void main() async {
  final storage = LocalStorage();
  await storage.initStorage();
  Environment.setEnvironment(EnvironmentEnum.PROD);
  runApp(MyApp());
}
