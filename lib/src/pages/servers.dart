import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/server_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_constants.dart';
import 'package:web_isolation_manager_front_end/src/core/providers/core_provider.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/dialogs.dart';
import 'package:web_isolation_manager_front_end/src/presenters/server.dart';
import 'package:web_isolation_manager_front_end/src/widgets/drawer.dart';

class ServersPage extends StatelessWidget {
  static final String routeName = 'servers-page';

  final _fieldSize = 320.0;
  final _bodyWidth = 550.0;
  final _fontSize = 15.0;
  final _buttonWidth = 150.0;
  final _buttonHeight = 40.0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    ServerBloc serverBloc = CoreProvider.serverBloc(context);
    serverBloc.getEntries(0);
    return Scaffold(
      key: _scaffoldKey,
      appBar: _createAppBar(),
      drawer: DrawerWidget(),
      body: _createBody(serverBloc, context),
    );
  }

  Container _createBody(ServerBloc serverBloc, BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.grey.shade200,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: _bodyWidth,
            color: Colors.grey.shade100,
            height: double.infinity,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: _createButtonBar(serverBloc, context),
                ),
                _createData(serverBloc),
              ],
            ),
          )
        ],
      ),
    );
  }

  StreamBuilder<Object> _createData(ServerBloc serverBloc) {
    return StreamBuilder<Object>(
        stream: serverBloc.serversStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: _generateServerList(serverBloc, snapshot.data, context),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  AppBar _createAppBar() {
    return AppBar(
      title: Center(
          child: Text(
        'Servidores',
        style: ThemeConstants.getCustomTitleStyle(20.0),
      )),
    );
  }

  Row _createButtonBar(ServerBloc serverBloc, context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _createAddButton(context, serverBloc),
      ],
    );
  }

  SizedBox _createAddButton(context, ServerBloc serverBloc) {
    return SizedBox(
      width: _buttonWidth,
      height: _buttonHeight,
      child: ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
          backgroundColor:
              MaterialStateProperty.all<Color>(ThemeColors.PRIMARY_COLOR),
        ),
        child: Text(
          'Agregar Servidor',
          style: ThemeConstants.getCustomTitleStyle(_fontSize - 1),
        ),
        onPressed: () async {
          serverBloc.setName('');
          serverBloc.setIp('');
          final action = await Dialogs.yesCancelDialog(
            context,
            'Nuevo',
            _createNewServer(serverBloc, true),
            yesText: 'Ok',
            cancelText: 'Cancelar',
          );
          if (action == DialogAction.yes) {
            await serverBloc.saveNew().catchError((error) {
              _showSnackBar(error, context);
            });
          }
        },
      ),
    );
  }

  Container _createNewServer(ServerBloc serverBloc, bool isNew) {
    return Container(
      height: _buttonWidth * 0.70,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _createNameField(serverBloc, isNew),
          _createIpField(serverBloc),
        ],
      ),
    );
  }

  StreamBuilder<Object> _createIpField(ServerBloc serverBloc) {
    return StreamBuilder<Object>(
        stream: serverBloc.ipStream,
        builder: (context, snapshot) {
          return Container(
            height: _buttonWidth / 3,
            padding: EdgeInsets.symmetric(vertical: 5),
            width: _fieldSize,
            child: TextFormField(
              initialValue: serverBloc.ip,
              onChanged: serverBloc.setIp,
              decoration: InputDecoration(
                border:
                    ThemeConstants.getBorderInputStyle(ThemeColors.TEXT_COLOR),
                errorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
                focusedErrorBorder:
                    ThemeConstants.getBorderInputStyle(Colors.red),
                hintText: 'IP',
                hintStyle: TextStyle(fontSize: 10.0),
                prefixIcon: Icon(
                  Icons.wifi_protected_setup,
                ),
              ),
            ),
          );
        });
  }

  StreamBuilder<Object> _createNameField(ServerBloc serverBloc, isNew) {
    return StreamBuilder<Object>(
        stream: serverBloc.nameStream,
        builder: (context, snapshot) {
          return Container(
            height: _buttonWidth * 0.3,
            padding: EdgeInsets.symmetric(vertical: 5),
            width: _fieldSize,
            child: TextFormField(
              initialValue: serverBloc.name,
              onChanged: serverBloc.setName,
              enabled: isNew,
              decoration: InputDecoration(
                border:
                    ThemeConstants.getBorderInputStyle(ThemeColors.TEXT_COLOR),
                errorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
                focusedErrorBorder:
                    ThemeConstants.getBorderInputStyle(Colors.red),
                hintText: 'Nombre',
                hintStyle: TextStyle(fontSize: 10.0),
                prefixIcon: Icon(
                  Icons.web,
                ),
              ),
            ),
          );
        });
  }

  List<Widget> _generateServerList(ServerBloc serverBloc, entries, context) {
    List<Widget> result = [];
    entries.forEach((Server server) {
      result.add(Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: InkWell(
          onTap: () async {
            serverBloc.setIp(server.ip);
            serverBloc.setName(server.name);
            serverBloc.selectedServer = server;
            final action = await Dialogs.modifyDeleteDialog(
              context,
              'Modificar o Elminar',
              _createNewServer(serverBloc, true),
              yesText: 'Guardar',
              cancelText: 'Eliminar',
            );
            if (action == DialogAction.yes) {
              await serverBloc.update().catchError((error) {
                _showSnackBar(error, context);
              });
            } else if (action == DialogAction.cancel) {
              await serverBloc.delete().catchError((error) {
                _showSnackBar(error, context);
              });
            }
          },
          child: Container(
            height: _buttonHeight * 1.75,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        server.name,
                        style: ThemeConstants.getTitleStyle(_fontSize - 1),
                      ),
                      Text(
                        server.ip,
                        style: TextStyle(fontSize: _fontSize - 4),
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Icon(
                    Icons.edit,
                    color: ThemeColors.PRIMARY_COLOR,
                  ),
                )
              ],
            ),
          ),
        ),
      ));
    });
    return result;
  }

  _showSnackBar(String message, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(milliseconds: 1500),
    ));
  }
}
