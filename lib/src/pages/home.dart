import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/entry_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/server_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_constants.dart';
import 'package:web_isolation_manager_front_end/src/core/providers/core_provider.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/dialogs.dart';
import 'package:web_isolation_manager_front_end/src/presenters/entry.dart';
import 'package:web_isolation_manager_front_end/src/widgets/drawer.dart';
import 'package:web_isolation_manager_front_end/src/widgets/server_drop_down.dart';

class HomePage extends StatelessWidget {
  static final String routeName = 'home-page';

  final _fieldSize = 320.0;
  final _bodyWidth = 550.0;
  final _fontSize = 15.0;
  final _buttonWidth = 150.0;
  final _buttonHeight = 40.0;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    EntryBloc entryBloc = CoreProvider.entryBloc(context);
    ServerBloc serverBloc = CoreProvider.serverBloc(context);
    entryBloc.getEntries(0);
    serverBloc.getEntries(0);
    return Scaffold(
      key: _scaffoldKey,
      appBar: _createAppBar(),
      drawer: DrawerWidget(),
      body: _createBody(entryBloc, serverBloc, context),
    );
  }

  Container _createBody(
      EntryBloc entryBloc, ServerBloc serverBloc, BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.grey.shade200,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: _bodyWidth,
            color: Colors.grey.shade100,
            height: double.infinity,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: _createButtonBar(
                    entryBloc,
                    serverBloc,
                    context,
                  ),
                ),
                _createData(entryBloc, serverBloc),
              ],
            ),
          )
        ],
      ),
    );
  }

  StreamBuilder<Object> _createData(
      EntryBloc entryBloc, ServerBloc serverBloc) {
    return StreamBuilder<Object>(
        stream: entryBloc.entriesStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: _generateEntryList(
                  entryBloc, serverBloc, snapshot.data, context),
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  AppBar _createAppBar() {
    return AppBar(
      title: Center(
          child: Text(
        'Sitios Aislados',
        style: ThemeConstants.getCustomTitleStyle(20.0),
      )),
    );
  }

  Row _createButtonBar(EntryBloc entryBloc, ServerBloc serverBloc, context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _createDeleteButton(entryBloc, context),
        _createAddButton(context, entryBloc, serverBloc),
      ],
    );
  }

  SizedBox _createAddButton(
      context, EntryBloc entryBloc, ServerBloc serverBloc) {
    return SizedBox(
      width: _buttonWidth,
      height: _buttonHeight,
      child: ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
          backgroundColor:
              MaterialStateProperty.all<Color>(ThemeColors.PRIMARY_COLOR),
        ),
        child: Text(
          'Agregar Sitio',
          style: ThemeConstants.getCustomTitleStyle(_fontSize - 1),
        ),
        onPressed: () async {
          entryBloc.setUrl('');
          entryBloc.setIp('');
          final action = await Dialogs.yesCancelDialog(
            context,
            'Nuevo',
            _createNewEntry(entryBloc, serverBloc, true),
            yesText: 'Ok',
            cancelText: 'Cancelar',
          );
          if (action == DialogAction.yes) {
            await entryBloc.saveNew().catchError((error) {
              _showSnackBar(error, context);
            });
          }
        },
      ),
    );
  }

  Container _createNewEntry(
      EntryBloc entryBloc, ServerBloc serverBloc, bool isNew) {
    return Container(
      height: _buttonWidth * 0.70,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _createUrlField(entryBloc, isNew),
          ServerDropDownWidget(serverBloc, entryBloc),
        ],
      ),
    );
  }

  StreamBuilder<Object> _createUrlField(EntryBloc entryBloc, isNew) {
    return StreamBuilder<Object>(
        stream: entryBloc.urlStream,
        builder: (context, snapshot) {
          return Container(
            height: _buttonWidth * 0.3,
            padding: EdgeInsets.symmetric(vertical: 5),
            width: _fieldSize,
            child: TextFormField(
              initialValue: entryBloc.url,
              onChanged: entryBloc.setUrl,
              enabled: isNew,
              decoration: InputDecoration(
                border:
                    ThemeConstants.getBorderInputStyle(ThemeColors.TEXT_COLOR),
                errorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
                focusedErrorBorder:
                    ThemeConstants.getBorderInputStyle(Colors.red),
                hintText: 'URL',
                hintStyle: TextStyle(fontSize: 10.0),
                prefixIcon: Icon(
                  Icons.web,
                ),
              ),
            ),
          );
        });
  }

  SizedBox _createDeleteButton(EntryBloc entryBloc, context) {
    return SizedBox(
      width: _buttonWidth,
      height: _buttonHeight,
      child: ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
        ),
        child: Text(
          'Eliminar Todos',
          style: ThemeConstants.getCustomTitleStyle(_fontSize - 1),
        ),
        onPressed: () async {
          final action = await Dialogs.yesCancelDialog(
              context,
              '¿Estás Seguro?',
              Text(
                'Se eliminarán todos los sitios',
                style: TextStyle(fontSize: 12.0),
              ));
          if (action == DialogAction.yes) {
            await entryBloc.deleteAll().catchError((error) {
              _showSnackBar(error, context);
            });
          }
        },
      ),
    );
  }

  List<Widget> _generateEntryList(
      EntryBloc entryBloc, ServerBloc serverBloc, entries, context) {
    List<Widget> result = [];
    entries.forEach((Entry entry) {
      result.add(Padding(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: InkWell(
          onTap: () async {
            entryBloc.setIp(entry.ip);
            entryBloc.setUrl(entry.url);
            final action = await Dialogs.modifyDeleteDialog(
              context,
              'Modificar o Elminar',
              _createNewEntry(entryBloc, serverBloc, false),
              yesText: 'Guardar',
              cancelText: 'Eliminar',
            );
            if (action == DialogAction.yes) {
              await entryBloc.update().catchError((error) {
                _showSnackBar(error, context);
              });
            } else if (action == DialogAction.cancel) {
              await entryBloc.delete().catchError((error) {
                _showSnackBar(error, context);
              });
            }
          },
          child: Container(
            height: _buttonHeight * 1.75,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        entry.url,
                        style: ThemeConstants.getTitleStyle(_fontSize - 1),
                      ),
                      Text(
                        entry.ip,
                        style: TextStyle(fontSize: _fontSize - 4),
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Icon(
                    Icons.edit,
                    color: ThemeColors.PRIMARY_COLOR,
                  ),
                )
              ],
            ),
          ),
        ),
      ));
    });
    return result;
  }

  _showSnackBar(String message, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(milliseconds: 1500),
    ));
  }
}
