import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/environment.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_constants.dart';

class IsolateMePage extends StatefulWidget {
  static final String routeName = 'isolate-me';

  @override
  _IsolateMePageState createState() => _IsolateMePageState();
}

class _IsolateMePageState extends State<IsolateMePage> {
  final _fieldSize = 320.0;
  String _url;

  @override
  void initState() {
    super.initState();
    _url = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(top: _fieldSize * 0.5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 30),
                child: Text(
                  'Isolate Me',
                  style: ThemeConstants.getTitleStyle(18.0),
                ),
              ),
              _createLabel(),
              _createUrlField(),
              _createButton(context),
            ],
          ),
        ),
      ),
    );
  }

  _createLabel() {
    return Text('Ingrese el sitio web al que desea acceder de forma segura');
  }

  _createUrlField() {
    return Container(
      width: _fieldSize,
      child: TextFormField(
        decoration: InputDecoration(
          border: ThemeConstants.getBorderInputStyle(ThemeColors.TEXT_COLOR),
          errorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
          focusedErrorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
          hintText: 'example.com',
          prefixIcon: Icon(
            Icons.person,
          ),
        ),
        onChanged: (value) {
          setState(() {
            _url = value;
          });
        },
      ),
    );
  }

  _createButton(BuildContext context) {
    return SizedBox(
      width: _fieldSize * 0.75,
      height: 50.0,
      child: ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
                if (states.contains(MaterialState.disabled))
                  return ThemeColors.TEXT_COLOR;
                else
                  return ThemeColors.PRIMARY_COLOR;
              },
            ),
          ),
          child: Text(
            'Ir',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
            ),
          ),
          onPressed: _url.trim().isNotEmpty
              ? () {
                  if (_url.trim().startsWith('http')) {
                    _showSnackBar(
                        'La dirección no cumple con el formato requerido',
                        context);
                    return;
                  }
                  if (!_url.trim().contains('\.')) {
                    _showSnackBar(
                        'La dirección no cumple con el formato requerido',
                        context);
                    return;
                  }
                  if (_url.trim().split('\.').length < 2) {
                    _showSnackBar(
                        'La dirección no cumple con el formato requerido',
                        context);
                    return;
                  }
                  if (_url.trim().split('\.').last.length < 2) {
                    _showSnackBar(
                        'La dirección no cumple con el formato requerido',
                        context);
                    return;
                  }
                  _launchTerms(_url.trim());
                }
              : null),
    );
  }

  _launchTerms(String baseUrl) async {
    String finalUrl =
        '${Environment.isolationApiUrl}/?url=https://$baseUrl?showTabs=false';
    if (await canLaunch(finalUrl)) {
      await launch(finalUrl);
    } else {
      throw 'No se pudo abrir http://$baseUrl';
    }
  }

  _showSnackBar(String message, BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(milliseconds: 1500),
    ));
  }
}
