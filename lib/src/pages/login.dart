import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/login_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_constants.dart';
import 'package:web_isolation_manager_front_end/src/core/providers/core_provider.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/exception.dart';
import 'package:web_isolation_manager_front_end/src/pages/home.dart';
import 'package:web_isolation_manager_front_end/src/pages/widgets/isolate_me.dart';

class LoginPage extends StatelessWidget {
  static final String routeName = 'login-page';
  @override
  Widget build(BuildContext context) {
    final _loginBloc = CoreProvider.loginBloc(context);
    return _Main(
      loginBloc: _loginBloc,
    );
  }
}

class _Main extends StatefulWidget {
  final LoginBloc loginBloc;

  _Main({
    @required this.loginBloc,
  });

  @override
  __Main createState() => __Main();
}

class __Main extends State<_Main> {
  final _fieldSize = 320.0;
  LoginBloc loginBloc;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  bool processing = false;
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    loginBloc = widget.loginBloc;
    usernameController.text = loginBloc.username ?? '';
    passwordController.text = loginBloc.password ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(top: _fieldSize * 0.5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 30),
                child: Text(
                  'Gestión de Servidor Web Isolation',
                  style: ThemeConstants.getTitleStyle(18.0),
                ),
              ),
              _createUsername(),
              _createPassword(),
              _createButton(context),
              Text('- o -'),
              _createIsolateButton(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _createButton(BuildContext context) {
    return StreamBuilder<Object>(
        stream: loginBloc.isFormValidStream,
        builder: (context, snapshot) {
          return SizedBox(
            width: _fieldSize * 0.75,
            height: 50.0,
            child: ElevatedButton(
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                ),
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) {
                    if (states.contains(MaterialState.disabled))
                      return ThemeColors.TEXT_COLOR;
                    else
                      return ThemeColors.PRIMARY_COLOR;
                  },
                ),
              ),
              child: Text(
                'Ingresar',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                ),
              ),
              onPressed: snapshot.hasData
                  ? () async {
                      if (usernameController.text.trim().isEmpty) {
                        final String error =
                            'El nombre de usuario es Obligatorio';
                        _showSnackBar(error);
                        throw error;
                      }
                      if (passwordController.text.trim().isEmpty) {
                        final String error = 'La contraseña es Obligatoria';
                        _showSnackBar(error);
                        throw error;
                      }
                      await loginBloc.login().catchError((error) {
                        _showSnackBar(ExceptionUtils.messageExceptions(error));
                        throw error;
                      });
                      Navigator.pushNamedAndRemoveUntil(
                          context, HomePage.routeName, (route) => false);
                    }
                  : null,
            ),
          );
        });
  }

  _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      duration: const Duration(milliseconds: 1500),
    ));
  }

  Widget _createPassword() {
    return StreamBuilder<Object>(
        stream: loginBloc.passwordStream,
        builder: (context, snapshot) {
          return Container(
            padding: EdgeInsets.only(bottom: 10),
            width: _fieldSize,
            child: TextFormField(
              onChanged: loginBloc.setPassword,
              controller: passwordController,
              decoration: InputDecoration(
                border:
                    ThemeConstants.getBorderInputStyle(ThemeColors.TEXT_COLOR),
                errorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
                focusedErrorBorder:
                    ThemeConstants.getBorderInputStyle(Colors.red),
                hintText: 'Contraseña',
                prefixIcon: Icon(
                  Icons.lock,
                ),
              ),
              obscureText: true,
            ),
          );
        });
  }

  Widget _createUsername() {
    return StreamBuilder<Object>(
        stream: loginBloc.usernameStream,
        builder: (context, snapshot) {
          return Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            width: _fieldSize,
            child: TextFormField(
              onChanged: loginBloc.setUsername,
              controller: usernameController,
              decoration: InputDecoration(
                border:
                    ThemeConstants.getBorderInputStyle(ThemeColors.TEXT_COLOR),
                errorBorder: ThemeConstants.getBorderInputStyle(Colors.red),
                focusedErrorBorder:
                    ThemeConstants.getBorderInputStyle(Colors.red),
                hintText: 'Usuario',
                prefixIcon: Icon(
                  Icons.person,
                ),
              ),
            ),
          );
        });
  }

  _createIsolateButton(BuildContext context) {
    return SizedBox(
      width: _fieldSize * 0.75,
      height: 50.0,
      child: ElevatedButton(
        style: ButtonStyle(
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
            (Set<MaterialState> states) {
              if (states.contains(MaterialState.disabled))
                return ThemeColors.TEXT_COLOR;
              else
                return ThemeColors.PRIMARY_COLOR;
            },
          ),
        ),
        child: Text(
          'Probar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          ),
        ),
        onPressed: () {
          Navigator.pushNamed(context, IsolateMePage.routeName);
        },
      ),
    );
  }
}
