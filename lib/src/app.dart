import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/routes.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/providers/core_provider.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/local_storage.dart';
import 'package:web_isolation_manager_front_end/src/pages/home.dart';
import 'package:web_isolation_manager_front_end/src/pages/login.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CoreProvider(
      child: MaterialApp(
        navigatorKey: Get.key,
        debugShowCheckedModeBanner: false,
        routes: getApplicationRoutes(),
        home: _getInitialRoute(),
        theme: ThemeData(
          primaryColor: ThemeColors.PRIMARY_COLOR,
          primaryColorLight: ThemeColors.PRIMARY_LIGHT_COLOR,
          backgroundColor: Colors.white,
          cardColor: Colors.white,
          textSelectionTheme: TextSelectionThemeData(
            cursorColor: ThemeColors.PRIMARY_COLOR,
            selectionColor: ThemeColors.PRIMARY_LIGHT_COLOR_OPACITY,
            selectionHandleColor: ThemeColors.PRIMARY_LIGHT_COLOR,
          ),
          scaffoldBackgroundColor: Colors.white,
        ),
      ),
    );
  }

  Widget _getInitialRoute() {
    if (LocalStorage().token != null && LocalStorage().token.isNotEmpty) {
      return HomePage();
    }
    return LoginPage();
  }
}
