// To parse this JSON data, do
//
//     final server = serverFromJson(jsonString);

import 'dart:convert';

Server serverFromJson(String str) => Server.fromJson(json.decode(str));

String serverToJson(Server data) => json.encode(data.toJson());

List<Server> servertPresenterFromDynamicList(List<dynamic> entities) {
  List<Server> list = [];
  entities.forEach((entity) => list.add(Server.fromJson(entity)));
  return list;
}

class Server {
  Server({
    this.name,
    this.ip,
    this.id,
  });

  String name;
  String ip;
  String id;

  factory Server.fromJson(Map<String, dynamic> json) => Server(
        name: json['name'],
        ip: json['ip'],
        id: json['id'],
      );

  Map<String, dynamic> toJson() => {
        'name': name,
        'ip': ip,
        'id': id,
      };
}
