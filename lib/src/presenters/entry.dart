// To parse this JSON data, do
//
//     final entry = entryFromJson(jsonString);

import 'dart:convert';

Entry entryFromJson(String str) => Entry.fromJson(json.decode(str));

String entryToJson(Entry data) => json.encode(data.toJson());

List<Entry> entrytPresenterFromDynamicList(List<dynamic> entities) {
  List<Entry> list = [];
  entities.forEach((entity) => list.add(Entry.fromJson(entity)));
  return list;
}

class Entry {
  Entry({
    this.url,
    this.ip,
  });

  String url;
  String ip;

  factory Entry.fromJson(Map<String, dynamic> json) => Entry(
        url: json['url'],
        ip: json['ip'],
      );

  Map<String, dynamic> toJson() => {
        'url': url,
        'ip': ip,
      };
}
