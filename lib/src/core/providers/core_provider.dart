import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/entry_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/login_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/server_bloc.dart';
import 'package:web_isolation_manager_front_end/src/services/entry_service.dart';
import 'package:web_isolation_manager_front_end/src/services/login_service.dart';
import 'package:web_isolation_manager_front_end/src/services/server_service.dart';

class CoreProvider extends InheritedWidget {
  final _loginBloc = LoginBloc(loginService: LoginService());
  final _entryBloc = EntryBloc(entryService: EntryService());
  final _serverBloc = ServerBloc(serverService: ServerService());

  static CoreProvider _instance;

  factory CoreProvider({Key key, Widget child}) {
    if (_instance == null) {
      _instance = new CoreProvider._(key: key, child: child);
    }

    return _instance;
  }

  CoreProvider._({Key key, @required Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static LoginBloc loginBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CoreProvider>()
        ._loginBloc;
  }

  static EntryBloc entryBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CoreProvider>()
        ._entryBloc;
  }

  static ServerBloc serverBloc(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CoreProvider>()
        ._serverBloc;
  }
}
