import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/exception.dart';
import 'package:web_isolation_manager_front_end/src/presenters/entry.dart';
import 'package:web_isolation_manager_front_end/src/services/entry_service.dart';

class EntryBloc {
  EntryService _entryService;

  EntryBloc({
    @required EntryService entryService,
  }) {
    _entryService = entryService;
  }

  final _loadingController = BehaviorSubject<bool>();
  final _entriesController = BehaviorSubject<List<Entry>>();
  final _urlController = BehaviorSubject<String>();
  final _ipController = BehaviorSubject<String>();

  Stream<bool> get loadingStream => _loadingController.stream;
  Stream<List<Entry>> get entriesStream => _entriesController.stream;
  Stream<String> get urlStream => _urlController.stream;
  Stream<String> get ipStream => _ipController.stream;

  Function(bool) get setLoading => _loadingController.sink.add;
  Function(List<Entry>) get setEntries => _entriesController.sink.add;
  Function(String) get setUrl => _urlController.sink.add;
  Function(String) get setIp => _ipController.sink.add;

  List<Entry> get entries => _entriesController.value;
  String get url => _urlController.value;
  String get ip => _ipController.value;

  Future<List<Entry>> getEntries(int page) async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      List<Entry> currentList = entries;
      if (page == 0) {
        currentList = [];
      }
      final resp = await _entryService.getEntries();
      currentList.addAll(resp);
      setEntries(currentList);
      setLoading(false);
      return resp;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> deleteAll() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp = await _entryService.deleteAll().catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> saveNew() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp = await _entryService.saveNew(url, ip).catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> update() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp = await _entryService.update(url, ip).catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> delete() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp = await _entryService.delete(url, ip).catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  dispose() {
    _loadingController?.close();
    _entriesController?.close();
    _urlController?.close();
    _ipController?.close();
  }
}
