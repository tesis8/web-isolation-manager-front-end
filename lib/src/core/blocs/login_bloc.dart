import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/streams.dart';
import 'package:rxdart/subjects.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/environment.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/exception.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/jwt_utils.dart';
import 'package:web_isolation_manager_front_end/src/services/login_service.dart';

class LoginBloc {
  LoginService _loginService;
  final _usernameController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  final _loadingController = BehaviorSubject<bool>();

  Stream<String> get usernameStream =>
      _usernameController.stream.transform(_validateUsername);
  Stream<String> get passwordStream =>
      _passwordController.stream.transform(_validatePassword);
  Stream<bool> get loadingStream => _loadingController.stream;

  Function(String) get setUsername => _usernameController.sink.add;
  Function(String) get setPassword => _passwordController.sink.add;
  Function(bool) get setLoading => _loadingController.sink.add;

  String get username => _usernameController.value;
  String get password => _passwordController.value;

  Stream<bool> get isFormValidStream => CombineLatestStream.combine2(
      usernameStream, passwordStream, (u, p) => true);

  LoginBloc({
    @required LoginService loginService,
  }) {
    this._loginService = loginService;
  }

  Future<void> login() async {
    try {
      setLoading(true);
      final resp = await _loginService
          .login(username.trim(), password.trim())
          .timeout(Environment.customTimeOut)
          .catchError((error) {
        setLoading(false);
        throw ExceptionUtils.messageExceptions(error);
      });
      if (resp['statusCode'] == '200') {
        if (resp['authorization'].toString().isNotEmpty) {
          JWTUtils.setTokenToStorage(resp['authorization'].toString());
          setUsername('');
          setPassword('');
        }
      } else if (resp['statusCode'] == '403') {
        setLoading(false);
        throw 'Credenciales Incorrectas';
      } else {
        setLoading(false);
        throw 'Un Error Inesperado ha Ocurrido';
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  logout() {
    JWTUtils.clearStorage();
  }

  final _validateUsername = StreamTransformer<String, String>.fromHandlers(
    handleData: (username, sink) => username.toString().trim().isNotEmpty
        ? sink.add(username)
        : sink.addError('El Nombre de Usuario es Obligatorio'),
  );

  final _validatePassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) => password.toString().trim().isNotEmpty
        ? sink.add(password)
        : sink.addError('La Contraseña es Obligatoria'),
  );

  dispose() {
    _usernameController?.close();
    _passwordController?.close();
    _loadingController?.close();
  }
}
