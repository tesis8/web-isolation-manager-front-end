import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/exception.dart';
import 'package:web_isolation_manager_front_end/src/presenters/server.dart';
import 'package:web_isolation_manager_front_end/src/services/server_service.dart';

class ServerBloc {
  ServerService _serverService;

  ServerBloc({
    @required ServerService serverService,
  }) {
    _serverService = serverService;
  }

  final _loadingController = BehaviorSubject<bool>();
  final _serversController = BehaviorSubject<List<Server>>();
  final _nameController = BehaviorSubject<String>();
  final _ipController = BehaviorSubject<String>();

  Stream<bool> get loadingStream => _loadingController.stream;
  Stream<List<Server>> get serversStream => _serversController.stream;
  Stream<String> get nameStream => _nameController.stream;
  Stream<String> get ipStream => _ipController.stream;

  Function(bool) get setLoading => _loadingController.sink.add;
  Function(List<Server>) get setEntries => _serversController.sink.add;
  Function(String) get setName => _nameController.sink.add;
  Function(String) get setIp => _ipController.sink.add;

  List<Server> get servers => _serversController.value;
  String get name => _nameController.value;
  String get ip => _ipController.value;
  Server selectedServer;

  Future<List<Server>> getEntries(int page) async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      List<Server> currentList = servers;
      if (page == 0) {
        currentList = [];
      }
      final resp = await _serverService.getServers();
      currentList.addAll(resp);
      setEntries(currentList);
      setLoading(false);
      return resp;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> saveNew() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp =
          await _serverService.saveNew(name, ip, null).catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> update() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp = await _serverService
          .update(name, ip, selectedServer.id)
          .catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  Future<bool> delete() async {
    try {
      if (_loadingController.value != null && _loadingController.value)
        return null;
      setLoading(true);
      final resp =
          await _serverService.delete(selectedServer.id).catchError((error) {
        throw error;
      });
      if (resp) {
        setLoading(false);
        getEntries(0);
      } else {
        throw Exception('Un error inesperado ha ocurrido');
      }
      setLoading(false);
      return true;
    } catch (error) {
      setLoading(false);
      throw ExceptionUtils.messageExceptions(error);
    }
  }

  dispose() {
    _loadingController?.close();
    _serversController?.close();
    _nameController?.close();
    _ipController?.close();
  }
}
