import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_constants.dart';

enum DialogAction { yes, cancel, close }
enum DialogFlow { row, column }

class Dialogs {
  static Future<DialogAction> yesCancelDialog(
    BuildContext context,
    String title,
    Widget body, {
    String yesText = 'Si',
    String cancelText = 'No',
  }) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Text(
              title,
              style: ThemeConstants.getTitleStyle(14.0),
            ),
            content: body,
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(DialogAction.cancel),
                child: Text(
                  cancelText,
                  style: TextStyle(color: ThemeColors.PRIMARY_COLOR),
                ),
              ),
              ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  backgroundColor: MaterialStateProperty.all<Color>(
                      ThemeColors.PRIMARY_COLOR),
                ),
                onPressed: () => Navigator.of(context).pop(DialogAction.yes),
                child: Text(
                  yesText,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          );
        });
    return (action != null) ? action : DialogAction.cancel;
  }

  static Future<DialogAction> modifyDeleteDialog(
    BuildContext context,
    String title,
    Widget body, {
    String yesText = 'Si',
    String cancelText = 'No',
  }) async {
    final action = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Row(
              children: [
                Text(
                  title,
                  style: ThemeConstants.getTitleStyle(14.0),
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.close,
                    color: ThemeColors.PRIMARY_COLOR,
                  ),
                  onPressed: () =>
                      Navigator.of(context).pop(DialogAction.close),
                ),
              ],
            ),
            content: body,
            actions: <Widget>[
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                ),
                onPressed: () => Navigator.of(context).pop(DialogAction.cancel),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    cancelText,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      ThemeColors.PRIMARY_COLOR),
                ),
                onPressed: () => Navigator.of(context).pop(DialogAction.yes),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    yesText,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          );
        });
    return (action != null) ? action : DialogAction.cancel;
  }
}
