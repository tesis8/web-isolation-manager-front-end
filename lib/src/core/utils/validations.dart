import 'dart:async';
import 'dart:collection';

class ValidationsUtils {
  static final emailRegexp = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  static final RegExp finalNameRegex = new RegExp(
      r'^(?![÷×])[a-zA-Z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF\s]*$');

  static final RegExp textAndNumberRegex = new RegExp(
      r'^(?![÷×])[a-zA-Z0-9\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u00FF\s]*$');

  static validateEmail() {
    return StreamTransformer<String, String>.fromHandlers(
      handleData: (email, sink) {
        if (email.isNotEmpty && emailRegexp.hasMatch(email)) {
          sink.add(email);
        } else {
          sink.addError('El correo posee un formato incorrecto');
        }
      },
    );
  }

  static validateSpecialChar() {
    return StreamTransformer<String, String>.fromHandlers(
        handleData: (value, sink) {
      if (value.isNotEmpty) {
        if (textAndNumberRegex.hasMatch(value)) {
          sink.add(value);
        } else {
          sink.addError('No se aceptan caracteres especiales.');
        }
      } else {
        sink.add(value);
      }
    });
  }

  static validateRequired() {
    return StreamTransformer<String, String>.fromHandlers(
        handleData: (value, sink) {
      if (value.isNotEmpty) {
        if (value.isNotEmpty && finalNameRegex.hasMatch(value)) {
          sink.add(value);
        } else {
          sink.addError('No se aceptan caracteres especiales.');
        }
      } else {
        sink.addError('El campo es obligatorio');
      }
    });
  }

  static validateTextRequired() {
    return StreamTransformer<String, String>.fromHandlers(
        handleData: (value, sink) {
      if (value.isNotEmpty) {
        sink.add(value);
      } else {
        sink.addError('El campo es obligatorio');
      }
    });
  }

  static validateName() {
    return StreamTransformer<String, String>.fromHandlers(
        handleData: (name, sink) {
      if (name.isNotEmpty) {
        if (name.isNotEmpty && finalNameRegex.hasMatch(name)) {
          sink.add(name);
        } else {
          sink.addError('El nombre no debe tener caracteres especiales.');
        }
      } else {
        sink.addError('El Nombre es obligatorio');
      }
    });
  }

  static validateLastName() {
    return StreamTransformer<String, String>.fromHandlers(
        handleData: (lastName, sink) {
      if (lastName.isNotEmpty) {
        if (lastName.isNotEmpty && finalNameRegex.hasMatch(lastName)) {
          sink.add(lastName);
        } else {
          sink.addError('El apellido no debe tener caracteres especiales.');
        }
      } else {
        sink.addError('El Apellido es obligatorio');
      }
    });
  }

  static validatePassword() {
    return StreamTransformer<String, String>.fromHandlers(
      handleData: (password, sink) {
        if (password.isNotEmpty && password.length >= 5) {
          sink.add(password);
        } else if (password.isEmpty) {
          sink.addError('La Contraseña es obligatoria');
        } else if (password.length < 5) {
          sink.addError('La Contraseña debe tener al menos 5 digitos');
        }
      },
    );
  }

  static validatePhone() {
    return StreamTransformer<String, String>.fromHandlers(
      handleData: (phone, sink) {
        final phoneTemp = getPhoneWithOutFormat(phone);
        if (phoneTemp.isNotEmpty && phoneTemp.length == 10) {
          sink.add(phone);
        } else {
          sink.addError('El teléfono debe contener 10 digitos');
        }
      },
    );
  }

  static String getPhoneWithOutFormat(String phone) {
    if (phone != null) {
      return phone
          .trim()
          .replaceAll('-', '')
          .replaceAll('+593', '0')
          .replaceAll(' ', '')
          .trim();
    }
    return '';
  }

  static Map<String, dynamic> validateSchedule(
      int startHour, int startMinute, int finishHour, int finishMinute) {
    Map<String, dynamic> resultMap = HashMap();
    if (startHour == null || finishHour == null) {
      resultMap.putIfAbsent('result', () => true);
      resultMap.putIfAbsent('message', () => 'Horario no disponible');
      return resultMap;
    }

    final listHours = _createRangeHourList(startHour, finishHour);
    String timeMessage =
        getStartFinishTime(startHour, startMinute, finishHour, finishMinute);
    DateTime currentTime = DateTime.now();

    if (currentTime.hour == startHour) {
      if (currentTime.minute >= startMinute) {
        resultMap.putIfAbsent('result', () => true);
        resultMap.putIfAbsent('message', () => '$timeMessage');
      } else {
        resultMap.putIfAbsent('result', () => false);
        resultMap.putIfAbsent('message',
            () => 'Abre a la(s) ${_getFullTime(startHour, startMinute)}');
      }
      return resultMap;
    }

    if (currentTime.hour == finishHour) {
      if (currentTime.minute < finishMinute) {
        resultMap.putIfAbsent('result', () => true);
        resultMap.putIfAbsent('message', () => '$timeMessage');
      } else {
        resultMap.putIfAbsent('result', () => false);
        resultMap.putIfAbsent('message', () => '$timeMessage');
      }
      return resultMap;
    }

    if (listHours.contains(currentTime.hour)) {
      resultMap.putIfAbsent('result', () => true);
      resultMap.putIfAbsent('message', () => '$timeMessage');
    } else {
      resultMap.putIfAbsent('result', () => false);
      resultMap.putIfAbsent('message', () => '$timeMessage');
    }

    return resultMap;
  }

  static List<int> _createRangeHourList(int startHour, int finishHour) {
    if (startHour > finishHour) finishHour = finishHour + 24;
    List<int> resultList = [];
    for (int i = startHour; i <= finishHour; i++) {
      resultList.add(i >= 24 ? (i - 24) : i);
    }
    return resultList;
  }

  static String _getTime(int time) {
    return time < 10 ? '0$time' : '$time';
  }

  static String _getHourTime(int hour) {
    return hour <= 12 ? '$hour' : '${hour - 12}';
  }

  static String _getTimeSystem(int hour) {
    return hour < 12 ? 'am' : 'pm';
  }

  static String _getFullTime(int hour, int minute) {
    return '${_getHourTime(hour)}:${_getTime(minute)}${_getTimeSystem(hour)}';
  }

  static String getStartFinishTime(
      int startHour, int startMinute, int finishHour, int finishMinute) {
    return '${_getFullTime(startHour, startMinute)} - ${_getFullTime(finishHour, finishMinute)}';
  }

  static LinkedHashMap<String, Set<String>> orderMap(Map unsorted) {
    LinkedHashMap<String, Set<String>> mapSorted = LinkedHashMap();
    mapSorted.putIfAbsent('Lunes', () => unsorted['Lunes']);
    mapSorted.putIfAbsent('Martes', () => unsorted['Martes']);
    mapSorted.putIfAbsent('Miércoles', () => unsorted['Miércoles']);
    mapSorted.putIfAbsent('Jueves', () => unsorted['Jueves']);
    mapSorted.putIfAbsent('Viernes', () => unsorted['Viernes']);
    mapSorted.putIfAbsent('Sábado', () => unsorted['Sábado']);
    mapSorted.putIfAbsent('Domingo', () => unsorted['Domingo']);

    mapSorted.keys
        .where((k) => mapSorted[k] == null)
        .toList()
        .forEach(mapSorted.remove);
    return mapSorted;
  }

  static String getDayName(String day) {
    switch (day) {
      case 'L':
        return 'Lunes';
        break;
      case 'M':
        return 'Martes';
        break;
      case 'X':
        return 'Miércoles';
        break;
      case 'J':
        return 'Jueves';
        break;
      case 'V':
        return 'Viernes';
        break;
      case 'S':
        return 'Sábado';
        break;
      case 'D':
        return 'Domingo';
        break;
      default:
        return '';
        break;
    }
  }
}
