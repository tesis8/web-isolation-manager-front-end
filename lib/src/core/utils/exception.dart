import 'dart:convert';

class ExceptionUtils {
  static String messageExceptions(error) {
    String message;
    try {
      message = error != null
          ? (error is String)
              ? error
              : (error is Map)
                  ? error['message'] ?? error['userMessage'] ?? error.toString()
                  : error.message ?? error.toString()
          : 'mensaje no disponible';
    } catch (e) {
      message = error.toString();
    }
    if (message.toLowerCase().contains('socketexception')) {
      message = 'socketexception';
    }
    if (message.toLowerCase().contains('future not completed')) {
      message = 'future not completed';
    }
    return message;
  }

  static ExceptionValidate dataExceptions(error) {
    ExceptionValidate result = ExceptionValidate();
    result.message = messageExceptions(error);
    result.conditionFailed = false;
    result.data = '';
    if (error is Map) {
      result.conditionFailed = error['conditionFailed'] ?? false;
      result.data = error['data'];
    }
    return result;
  }

  static Map<String, dynamic> getMapValue(String resource,
      {List<int> bodyBytes}) {
    if (resource.contains('{', 0)) {
      Map<String, dynamic> responseError =
          json.decode(bodyBytes != null ? utf8.decode(bodyBytes) : resource);
      return responseError;
    }
    return null;
  }

  static String verifyErrorString(String error) {
    if (error.contains('<body>')) {
      return 'Algo salió mal';
    }
    return error;
  }
}

class ExceptionValidate {
  bool conditionFailed;
  String message;
  dynamic data;

  ExceptionValidate({
    this.message,
    this.data,
  });
}
