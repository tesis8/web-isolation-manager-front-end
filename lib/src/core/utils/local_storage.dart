import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:web_isolation_manager_front_end/src/pages/login.dart';

class LocalStorage {
  static final LocalStorage _instance = new LocalStorage._();

  factory LocalStorage() {
    return _instance;
  }

  LocalStorage._();

  SharedPreferences _storage;

  initStorage() async {
    _storage = await SharedPreferences.getInstance();
  }

  String get token => _storage.getString('token') ?? '';

  set token(String token) => _storage.setString('token', token);

  get userId => _storage.getString('userId') ?? '';

  set userId(String userId) => _storage.setString('userId', userId);

  static void cleanStorage() {
    LocalStorage().token = null;
    LocalStorage().userId = null;
    Get.offAll(LoginPage());
  }
}
