import 'package:flutter/cupertino.dart';
import 'package:web_isolation_manager_front_end/src/pages/home.dart';
import 'package:web_isolation_manager_front_end/src/pages/login.dart';
import 'package:web_isolation_manager_front_end/src/pages/servers.dart';
import 'package:web_isolation_manager_front_end/src/pages/widgets/isolate_me.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    LoginPage.routeName: (context) => LoginPage(),
    HomePage.routeName: (context) => HomePage(),
    ServersPage.routeName: (context) => ServersPage(),
    IsolateMePage.routeName: (context) => IsolateMePage(),
  };
}
