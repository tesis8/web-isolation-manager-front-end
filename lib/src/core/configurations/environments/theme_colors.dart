import 'package:flutter/cupertino.dart';

class ThemeColors {
  static const COLOR_CUSTOM_BLUE = Color.fromRGBO(0, 87, 126, 1.0);
  static const COLOR_CUSTOM_BLUE_END = Color.fromRGBO(0, 47, 126, 1.0);
  static const COLOR_CUSTOM_BLUE_OPACITY = Color.fromRGBO(0, 87, 126, 0.9);
  static const COLOR_CUSTOM_BLUE_ACCENT = Color.fromRGBO(39, 121, 158, 0.7);
  static const PRIMARY_COLOR = Color.fromRGBO(0, 74, 97, 1.0);
  static const PRIMARY_LIGHT_COLOR = Color.fromRGBO(0, 180, 172, 1.0);
  static const PRIMARY_LIGHT_COLOR_OPACITY = Color.fromRGBO(213, 237, 240, 1.0);
  static const PRIMARY_LIGHT_COLOR_OPACITY_2 =
      Color.fromRGBO(164, 236, 232, 1.0);
  static const PRIMARY_LIGHT_COLOR_OPACITY_4 =
      Color.fromRGBO(92, 234, 221, 0.4);
  static const PRIMARY_LIGHTER_COLOR = Color.fromRGBO(92, 234, 221, 1.0);
  static const PRIMARY_LIGHT_COLOR_OPACITY_5 = Color.fromRGBO(0, 180, 172, 0.8);
  static const TEXT_COLOR = Color.fromRGBO(189, 189, 189, 1.0);
  static const TEXT_BLACK_COLOR = Color.fromRGBO(51, 51, 51, 1.0);
  static const ERROR_COLOR = Color.fromRGBO(244, 129, 32, 1.0);
  static const FACEBOOK_COLOR = Color.fromRGBO(47, 128, 237, 1.0);
  static const GOOGLE_COLOR = Color.fromRGBO(235, 87, 87, 1.0);
  static const APPLE_BLACK_COLOR = Color.fromRGBO(0, 0, 0, 1.0);
  static const APPLE_SILVER_COLOR = Color.fromRGBO(163, 170, 174, 1.0);
  static const APPLE_WHITE_COLOR = Color.fromRGBO(255, 255, 255, 1.0);
  static const WARNING_COLOR = Color.fromRGBO(255, 210, 0, 1.0);
  static const BACKGROUND_COLOR = Color.fromRGBO(237, 243, 243, 1.0);
  static const DIVIDER_COLOR = Color.fromRGBO(224, 224, 224, 1.0);
  static const TEXT_COLOR_GRAY = Color.fromRGBO(130, 130, 130, 1.0);
  static const BLUE_COLOR_30 = Color.fromRGBO(164, 236, 232, 1.0);
  static const GRAY_COLOR_6 = Color.fromRGBO(242, 242, 242, 1.0);

  static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }
}
