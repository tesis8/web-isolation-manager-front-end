import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';

class ThemeConstants {
  static const double REM = 2.0;
  static const double HORIZONTAL_PADDING = 24.0;
  static const TextStyle TITLE_STYLE =
      TextStyle(color: ThemeColors.PRIMARY_COLOR, fontWeight: FontWeight.w600);
  static const TextStyle SUBTITLE_STYLE = TextStyle(
      color: ThemeColors.PRIMARY_LIGHT_COLOR, fontWeight: FontWeight.w600);

  static TextStyle getTitleStyle(double fontSize) {
    return TextStyle(
        color: ThemeColors.PRIMARY_COLOR,
        fontWeight: FontWeight.bold,
        fontSize: fontSize);
  }

  static TextStyle getUnderlinedTitleStyle(double fontSize) {
    return TextStyle(
      color: ThemeColors.PRIMARY_COLOR,
      fontWeight: FontWeight.bold,
      fontSize: fontSize,
      decoration: TextDecoration.underline,
    );
  }

  static TextStyle getSubTitleStyle(double fontSize) {
    return TextStyle(
        color: ThemeColors.PRIMARY_LIGHT_COLOR,
        fontWeight: FontWeight.w600,
        fontSize: fontSize);
  }

  static TextStyle getParagraphStyle(double fontSize) {
    return TextStyle(
        color: ThemeColors.TEXT_COLOR,
        fontWeight: FontWeight.w600,
        fontSize: fontSize);
  }

  static InputBorder getBorderInputStyle(Color borderColor) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(8.0),
      borderSide: BorderSide(color: borderColor, width: 1.0),
    );
  }

  static InputBorder getStadiumBorderInputStyle(Color borderColor) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(500.0),
      borderSide: BorderSide(color: borderColor, width: 1.0),
    );
  }

  static TextStyle getCustomTitleStyle(double fontSize, [Color color]) {
    return TextStyle(
        color: color ?? Colors.white,
        fontWeight: FontWeight.w600,
        fontSize: fontSize);
  }

  static TextStyle getCustomSubTitleStyle(double fontSize, [Color color]) {
    return TextStyle(
        color: color ?? Colors.white,
        fontWeight: FontWeight.w500,
        fontSize: fontSize);
  }

  static TextStyle getCustomStyleText(double fontSize,
      {Color color, FontWeight weight}) {
    return TextStyle(
        color: color ?? ThemeColors.TEXT_COLOR_GRAY,
        fontWeight: weight ?? FontWeight.w600,
        fontSize: fontSize);
  }
}
