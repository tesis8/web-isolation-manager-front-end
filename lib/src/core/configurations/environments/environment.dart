enum EnvironmentEnum { DEV, PROD }

class Environment {
  static Map<String, dynamic> _config;

  static void setEnvironment(EnvironmentEnum env) {
    switch (env) {
      case EnvironmentEnum.DEV:
        _config = _Config.devConstants;
        break;
      case EnvironmentEnum.PROD:
        _config = _Config.prodConstants;
        break;
    }
  }

  static get environment {
    return _config[_Config.environment];
  }

  static get rem {
    return _config[_Config.REM];
  }

  static get establishmentPage {
    return _config[_Config.ESTABLISHMENTS_PAGE];
  }

  static get customTimeOut {
    return _config[_Config.CUSTOM_TIMEOUT];
  }

  static get itemsPerPage {
    return _config[_Config.ITEMS_PAGE];
  }

  static get defaultDecimals {
    return _config[_Config.DEFAULT_DECIMALS];
  }

  static get notImage {
    return _config[_Config.NOT_IMAGE];
  }

  static int get maxThumbnailCount {
    return _config[_Config.MAXTHUMBNAILCOUNT];
  }

  static get baseUrl {
    return _config[_Config.BASE_URL];
  }

  static String get assetLoading {
    return _config[_Config.LOADING];
  }

  static get isolationApiUrl {
    return _config[_Config.ISOLATION_API_URL];
  }
}

class _Config {
  static const environment = 'environment';
  static const REM = 'REM';
  static const ITEMS_PAGE = 'ITEMS_PAGE';
  static const ESTABLISHMENTS_PAGE = 'ESTABLISHMENTS_PAGE';
  static const CUSTOM_TIMEOUT = 'CUSTOM_TIMEOUT';
  static const MAXTHUMBNAILCOUNT = 'MAXTHUMBNAILCOUNT';
  static const DEFAULT_DECIMALS = 'DEFAULT_DECIMALS';

  static const BASE_URL = 'BASE_URL';
  static const ISOLATION_API_URL = 'ISOLATION_API_URL';
  static const NOT_IMAGE = 'NOT_IMAGE';
  static const LOADING = 'LOADING';

  static Map<String, dynamic> devConstants = {
    environment: 'local',
    REM: 2.0,
    ITEMS_PAGE: 10,
    ESTABLISHMENTS_PAGE: 25,
    CUSTOM_TIMEOUT: Duration(seconds: 10),
    MAXTHUMBNAILCOUNT: 3,
    DEFAULT_DECIMALS: 2,
    BASE_URL: 'http://192.168.1.107:8080',
    ISOLATION_API_URL: 'http://192.168.1.107:8002',
    NOT_IMAGE: 'assets/images/no-image.png',
    LOADING: 'assets/images/gifs/loading.gif',
  };

  static Map<String, dynamic> prodConstants = {
    environment: 'prod',
    REM: 2.0,
    ITEMS_PAGE: 10,
    ESTABLISHMENTS_PAGE: 25,
    CUSTOM_TIMEOUT: Duration(seconds: 10),
    MAXTHUMBNAILCOUNT: 3,
    DEFAULT_DECIMALS: 2,
    BASE_URL: 'http://BACKEND_IP:BACKEND_PORT/BACKEND_CONTEXT',
    ISOLATION_API_URL: 'http://BACKEND_IP:8002',
    NOT_IMAGE: 'assets/images/no-image.png',
    LOADING: 'assets/images/gifs/loading.gif',
  };
}
