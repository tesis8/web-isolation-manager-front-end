import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_colors.dart';
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/theme_constants.dart';
import 'package:web_isolation_manager_front_end/src/core/providers/core_provider.dart';
import 'package:web_isolation_manager_front_end/src/pages/home.dart';
import 'package:web_isolation_manager_front_end/src/pages/login.dart';
import 'package:web_isolation_manager_front_end/src/pages/servers.dart';

class DrawerWidget extends StatelessWidget {
  final _fontSize = 15.0;
  const DrawerWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _createDrawer(context),
    );
  }

  Drawer _createDrawer(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 150.0,
            color: ThemeColors.PRIMARY_COLOR,
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, ServersPage.routeName, (route) => false);
            },
            child: _createServerMenu(),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, HomePage.routeName, (route) => false);
            },
            child: _createSiteMenu(),
          ),
          Spacer(),
          InkWell(
            onTap: () {
              CoreProvider.loginBloc(context).logout();
              Navigator.pushNamedAndRemoveUntil(
                  context, LoginPage.routeName, (route) => false);
            },
            child: _createLogout(),
          ),
        ],
      ),
    );
  }

  Padding _createLogout() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'Cerrar Sesión',
            style: ThemeConstants.getTitleStyle(_fontSize),
          ),
          Icon(
            Icons.exit_to_app,
            color: ThemeColors.PRIMARY_COLOR,
          ),
        ],
      ),
    );
  }

  Padding _createServerMenu() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'Servidores',
            style: ThemeConstants.getTitleStyle(_fontSize),
          ),
          Icon(
            Icons.settings,
            color: ThemeColors.PRIMARY_COLOR,
          ),
        ],
      ),
    );
  }

  Padding _createSiteMenu() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'Sitios Aislados',
            style: ThemeConstants.getTitleStyle(_fontSize),
          ),
          Icon(
            Icons.playlist_add_rounded,
            color: ThemeColors.PRIMARY_COLOR,
          ),
        ],
      ),
    );
  }
}
