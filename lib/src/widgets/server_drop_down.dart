import 'package:flutter/material.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/entry_bloc.dart';
import 'package:web_isolation_manager_front_end/src/core/blocs/server_bloc.dart';
import 'package:web_isolation_manager_front_end/src/presenters/server.dart';

class ServerDropDownWidget extends StatefulWidget {
  final ServerBloc serverBloc;
  final EntryBloc entryBloc;
  ServerDropDownWidget(this.serverBloc, this.entryBloc);

  @override
  _ServerDropDownWidgetState createState() => _ServerDropDownWidgetState();
}

class _ServerDropDownWidgetState extends State<ServerDropDownWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<dynamic>> items = [];
    return StreamBuilder(
      stream: widget.serverBloc.serversStream,
      builder: (context, AsyncSnapshot<List<Server>> snapshot) {
        if (items == null || items.isEmpty) {
          items = _generateServerOptions(snapshot.data);
        }
        return Container(
          alignment: Alignment.center,
          child: DropdownButton(
              isExpanded: true,
              items: items,
              value: widget.serverBloc.selectedServer,
              hint: Text('Servidor'),
              onChanged: (value) {
                setState(() {
                  widget.serverBloc.selectedServer = value;
                  widget.entryBloc.setIp(value.ip);
                });
              }),
        );
      },
    );
  }

  List<DropdownMenuItem> _generateServerOptions(List<Server> servers) {
    List<DropdownMenuItem> result = [];
    if (servers != null) {
      servers.forEach((server) {
        result.add(DropdownMenuItem(value: server, child: Text(server.name)));
      });
    } else {
      return null;
    }
    return result;
  }
}
