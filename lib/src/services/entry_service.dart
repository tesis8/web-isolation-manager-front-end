import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/environment.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/local_storage.dart';
import 'package:web_isolation_manager_front_end/src/presenters/entry.dart';

class EntryService {
  Future<List<Entry>> getEntries() async {
    final finalUrl = '${Environment.baseUrl}/entries';
    final resp = await http.get(
      finalUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "${LocalStorage().token}"
      },
    ).catchError((error) {
      throw error;
    });
    if (resp.statusCode == 200) {
      List<dynamic> response = json.decode(utf8.decode(resp.bodyBytes));
      return entrytPresenterFromDynamicList(response);
    } else if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    } else {
      throw resp.statusCode.toString();
    }
  }

  Future<bool> deleteAll() async {
    final finalUrl = '${Environment.baseUrl}/entries';
    final resp = await http.delete(
      finalUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "${LocalStorage().token}"
      },
    ).catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }

  Future<bool> saveNew(String url, String ip) async {
    var body = jsonEncode({'url': '$url', 'ip': '$ip'});
    final finalUrl = '${Environment.baseUrl}/entry';
    final resp = await http
        .post(finalUrl,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "${LocalStorage().token}"
            },
            body: body)
        .catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }

  Future<bool> update(String url, String ip) async {
    var body = jsonEncode({'url': '$url', 'ip': '$ip'});
    final finalUrl = '${Environment.baseUrl}/entry';
    final resp = await http
        .put(finalUrl,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "${LocalStorage().token}"
            },
            body: body)
        .catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }

  Future<bool> delete(String url, String ip) async {
    final finalUrl = '${Environment.baseUrl}/entry?url=$url&ip=$ip';
    final resp = await http.delete(
      finalUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "${LocalStorage().token}"
      },
    ).catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }
}
