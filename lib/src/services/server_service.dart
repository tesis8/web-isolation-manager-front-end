import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/environment.dart';
import 'package:web_isolation_manager_front_end/src/core/utils/local_storage.dart';
import 'package:web_isolation_manager_front_end/src/presenters/server.dart';

class ServerService {
  Future<List<Server>> getServers() async {
    final finalUrl = '${Environment.baseUrl}/servers';
    final resp = await http.get(
      finalUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "${LocalStorage().token}"
      },
    ).catchError((error) {
      throw error;
    });
    if (resp.statusCode == 200) {
      List<dynamic> response = json.decode(utf8.decode(resp.bodyBytes));
      return servertPresenterFromDynamicList(response);
    } else if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    } else {
      throw resp.statusCode.toString();
    }
  }

  Future<bool> saveNew(String name, String ip, String id) async {
    var body = jsonEncode({'name': '$name', 'ip': '$ip', 'id': '$id'});
    final finalUrl = '${Environment.baseUrl}/server';
    final resp = await http
        .post(finalUrl,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "${LocalStorage().token}"
            },
            body: body)
        .catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }

  Future<bool> update(String name, String ip, String id) async {
    var body = jsonEncode({'name': '$name', 'ip': '$ip', 'id': '$id'});
    final finalUrl = '${Environment.baseUrl}/server';
    final resp = await http
        .put(finalUrl,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "${LocalStorage().token}"
            },
            body: body)
        .catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }

  Future<bool> delete(String id) async {
    final finalUrl = '${Environment.baseUrl}/server?id=$id';
    final resp = await http.delete(
      finalUrl,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "${LocalStorage().token}"
      },
    ).catchError((error) {
      throw error;
    });
    if (resp.statusCode == 403) {
      LocalStorage.cleanStorage();
      throw Exception('Unauthorized');
    }
    return resp.statusCode == 200;
  }
}
