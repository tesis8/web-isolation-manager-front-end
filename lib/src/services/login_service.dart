import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:web_isolation_manager_front_end/src/core/configurations/environments/environment.dart';

class LoginService {
  Future<Map<String, dynamic>> login(username, password) async {
    var body = jsonEncode({'username': '$username', 'password': '$password'});
    final resp = await http
        .post(
      '${Environment.baseUrl}/login',
      headers: {"Content-Type": "application/json"},
      body: body,
    )
        .catchError((error) {
      throw error;
    });
    var returned = {'statusCode': resp.statusCode.toString()};
    returned.addAll(resp.headers);
    return returned;
  }
}
