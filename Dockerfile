FROM nginx:alpine
COPY build/web/ /usr/share/nginx/html/
COPY ./cmd.sh /docker-entrypoint.d/40-replace-ip.sh
